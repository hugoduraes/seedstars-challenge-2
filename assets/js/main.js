(function () {
  'use strict';

  var slideout = new Slideout({
    panel: document.getElementById('panel'),
    menu: document.getElementById('menu'),
    padding: 294,
    tolerance: 70
  });

  // Toggle button
  var toggleButton = function () {
    slideout.toggle();
  };

  document.querySelector('.menu-button').addEventListener('click', toggleButton);
  document.querySelector('.overlay').addEventListener('click', toggleButton);
})();
