# Seedstars Challenge #2

Webpage design.

## Assumptions

**Browser Compatibility**

  No requirements were established for browser compatibility, so I can only
  assure everything is working as supposed on tested browsers: Google Chrome 48
  (desktop and iOS), Safari 9.0.3 (Mac OS X) and Mobile Safari 9 (iOS).

**Slideout Menu**

  For the slideout menu I've used a third-party library: [Slideout.js](https://mango.github.io/slideout/).

## Usage

```
# clone repository
git clone https://hugoduraes@bitbucket.org/hugoduraes/seedstars-challenge-2.git

# change directory
cd seedstars-challenge-2

# install dependencies
npm install

# start app
npm start
```

*Visit [http://localhost:8080]() to view the page.*
