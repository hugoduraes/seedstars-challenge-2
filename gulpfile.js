'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var less = require('gulp-less');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

// Less plugins
var LessPluginCleanCss = require('less-plugin-clean-css');
var LessPluginAutoPrefix = require('less-plugin-autoprefix');
var cleancss = new LessPluginCleanCss({ advanced: true });
var autoprefix = new LessPluginAutoPrefix({ browsers: ['last 2 versions'] });

gulp.task('less', function () {
  return gulp.src('assets/less/index.less')
    .pipe(less({
      plugins: [autoprefix, cleancss]
    }))
    .pipe(rename('styles.css'))
    .pipe(gulp.dest('public/css'))
  ;
});

gulp.task('js', function () {
  return gulp.src([
    'node_modules/slideout/dist/slideout.js',
    'assets/js/**/*.js'
  ])
  .pipe(concat('bundle.js'))
  .pipe(uglify())
  .pipe(gulp.dest('public/js'));
});

gulp.task('images', function () {
  return gulp.src('assets/images/**/*.{jpg,jpeg,png,gif,svg}')
    .pipe(imagemin({
      optimizationLevel: 3, // png
      progressive: true, // jpg
      interlaced: true // gif
    }))
    .pipe(gulp.dest('public/images'))
  ;
});

gulp.task('fonts', function () {
  return gulp.src('assets/fonts/**')
    .pipe(gulp.dest('public/fonts'))
  ;
});

gulp.task('build', ['less', 'js', 'images', 'fonts']);

gulp.task('default', ['build'], function () {
  gulp.watch('assets/less/**/*.less', ['less']);
  gulp.watch('assets/js/**/*.js', ['js']);
  gulp.watch('assets/images/**/*.{jpg,jpeg,png,gif,svg}', ['images']);
  gulp.watch('assets/fonts/**', ['fonts']);
});
